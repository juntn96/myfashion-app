import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import styles from "./styles";

class FashionItem extends Component {

  _onPress = () => {
    const { onPress, item } = this.props
    if (onPress) {
      onPress(item)
    } 
  }

  render() {
    const { item } = this.props;

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={this._onPress} style={styles.container}>
        <View>
          <Image
            source={item.image}
            resizeMode={"contain"}
            style={styles.image}
          />
          {item.salePrice ? (
            <Image style={styles.imageSale} source={require("./ic_sale.png")} />
          ) : null}
        </View>
        <Text style={styles.textName}>{item.name}</Text>
        <View style={styles.containerDetail}>
          <Text style={styles.textPrice}>
            {item.currency}
            {item.price}
          </Text>
          {item.salePrice ? (
            <Text style={styles.textSale}>
              {item.currency} {item.salePrice}
            </Text>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  }
}

export default FashionItem;
