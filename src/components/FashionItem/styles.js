import { StyleSheet, Dimensions } from "react-native";
import materials from "../../theme/variables/material";

export default StyleSheet.create({
  container: {
    marginTop: 22 / 3,
    marginLeft: 26 / 2,
    marginBottom: 22 / 3,
  },
  image: {
    width: Dimensions.get("screen").width / 2 - 20,
    height: Dimensions.get("screen").width / 2 - 20,
  },
  textName: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#00BF9D",
    marginBottom: 4,
    marginTop: 4,
  },
  containerDetail: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
  },
  textPrice: {
    fontSize: 17,
    fontWeight: "400",
    color: '#000',
    marginRight: 12,
  },
  textSale: {
    fontSize: 15,
    color: '#808080',
    textDecorationLine: "line-through",
  },
  imageSale: {
    width: 30,
    height: 30,
    position: "absolute",
    bottom: 12,
    right: 8,
  },
});
