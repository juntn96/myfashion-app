import React, { Component } from "react";
import { TouchableOpacity } from "react-native";
import { ListItem, Text } from "native-base";
import Icon from "../../../elements/Icon";
import styles from "./styles";

class SideBarItem extends Component {
  
  _onPress = () => {
    const { onPress, item } = this.props
    if (onPress) {
      onPress(item)
    }
  }

  render() {

    const item = this.props.item

    return (
      <ListItem>
        <TouchableOpacity onPress={this._onPress} style={styles.container}>
          <Icon style={styles.icon} name={item.icon} />
          <Text style={styles.text}>{item.name}</Text>
        </TouchableOpacity>
      </ListItem>
    );
  }
}

export default SideBarItem;
