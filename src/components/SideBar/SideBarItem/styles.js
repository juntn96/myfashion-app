import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
  icon: {
    color: "#FFF",
  },
  text: {
    marginLeft: 12,
    fontSize: 22,
    color: "#FFF",
    fontWeight: "400",
  },
});
