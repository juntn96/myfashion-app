import React, { PureComponent } from "react";
import { Image, TouchableOpacity, Alert, Dimensions, FlatList } from "react-native";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-picker";
import { Content, Text, ListItem, Left, View, List } from "native-base";

import SideBarItem from "./SideBarItem";

import * as authActions from "../../store/actions/auth";
import * as commonActions from "../../store/actions/common";
import { getRouter } from "../../store/selectors/common";
import images from "../../assets/images";
import Icon from "../../elements/Icon";

import options from "./options";
import styles from "./styles";

const imagePickerOptions = {
  title: "Select Avatar",
  customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images",
  },
};
@connect(
  state => ({
    router: getRouter(state).current,
    user: state.auth.user,
  }),
  { ...authActions, ...commonActions }
)
export default class extends PureComponent {
  onFanProfilePress() {
    const { forwardTo, closeDrawer } = this.props;
    closeDrawer();
    forwardTo("fanProfile");
  }

  _handleSuccessLogout() {
    const { forwardTo, setToast, removeAllCampaign } = this.props;
    // OneSignal.deleteTag("user_id")
    removeAllCampaign();
    forwardTo("login");
    setToast("Logout successfully!!!");
  }

  _handleFailLogout(error) {
    const { forwardTo, setToast, removeAllCampaign } = this.props;
    // OneSignal.deleteTag("user_id")
    removeAllCampaign();
    forwardTo("login");
    setToast(error.msg, "error");
  }

  navigateTo(route) {
    const { forwardTo, closeDrawer } = this.props;
    closeDrawer();
    forwardTo(route);
  }

  changeAvatar() {
    ImagePicker.showImagePicker(imagePickerOptions, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: `data:image/jpeg;base64,${response.data}` };
        Alert.alert(
          "Change Avatar successfull",
          `Pick avatar successfull uri: ${source.uri}`,
          [
            {
              title: "Ok",
            },
          ]
        );

        // this.setState({
        //   avatarSource: source
        // });
      }
    });
  }

  _keyExtractor = item => item.name

  _renderRow = ({item}) => {
    const { user, logout } = this.props;
    if (item.name === "Logout") {
      if (user) {
        return <SideBarItem item={item} onPress={() => logout()} />;
      } else {
        return null
      }
    } else {
      return <SideBarItem item={item} onPress={() => this.navigateTo(item.route)} />
    }
  };

  render() {
    const { router, user } = this.props;
    console.log('>>>> ', user)
    return (
      <Content>
        <View style={styles.container}>
          <Image
            style={styles.background}
            source={require("./sidebarbg.jpg")}
          />
          <FlatList
            data={options}
            extraData={user}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderRow}
            contentContainerStyle={styles.list}
          />
          <View style={styles.footer}>
            <View style={styles.avatar} />
            <Text style={styles.text}>{user ? user.email : 'Account'}</Text>
          </View>
        </View>
      </Content>
    );
  }
}
