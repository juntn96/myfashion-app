export default [
  {
    name: "Home",
    route: "home",
    icon: "home",
  },
  {
    name: "Search",
    route: "search",
    icon: "search",
  },
  {
    name: "Categories",
    route: "category",
    icon: "pricetags",
  },
  {
    name: "Basket",
    route: "basket",
    icon: "basket",
  },
  {
    name: "Logout",
    icon: "exit",
  },
];
  