import { StyleSheet, Dimensions, Platform } from "react-native";
import material from "../../theme/variables/material";

export default StyleSheet.create({
  container: {
    backgroundColor: "#00BF9D",
    opacity: 0.8,
    height: Dimensions.get("screen").height - 72,
    justifyContent: "center",
  },
  background: {
    position: "absolute",
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
  },
  list: {
    flex: 1,
    justifyContent: "center"
  },
  footer: {
    flexDirection: "row",
    marginBottom: Platform.OS === 'ios' ? 0 : 24 ,
    marginLeft: 16,
    marginRight: 8,
    alignItems: "center",
  },
  avatar: {
    width: 80,
    height: 80,
    backgroundColor: "#FFF",
    borderRadius: 80,
    marginRight: 12,
  },
  text: {
    color: "#FFF",
    fontSize: 18,
    fontWeight: "400",
  },
});
