import React, { Component } from "react";
import { View, Text } from "react-native";
import { ListItem, Thumbnail } from "native-base";
import Icon from "../../../elements/Icon";
import styles from "./styles";

class BasketItem extends Component {
  render() {
    return (
      <ListItem>
        <View style={styles.container}>
          <Thumbnail
            square
            source={require("../../../assets/images/quan1.jpg")}
            large
          />
          <View style={styles.containerInfo}>
            <Text style={styles.textName}>Dress Helena</Text>
            <View style={styles.containerColorNSize}>
              <View style={styles.color} />
              <Text style={styles.textColorNSize}>Black, M</Text>
            </View>
            <View style={styles.containerEdit}>
              <View style={styles.dropdown}>
                <Text>1</Text>
                <Icon name={"ios-arrow-down"} style={styles.icon} />
              </View>
              <Icon name={"ios-create-outline"} style={styles.icon} />
              <Icon name={"ios-trash-outline"} style={styles.icon} />
            </View>
          </View>
          <View style={styles.containerPrice}>
            <Text>$120</Text>
          </View>
        </View>
      </ListItem>
    );
  }
}

export default BasketItem;
