import { StyleSheet, Dimensions, Platform } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  containerInfo: {
    marginLeft: 14,
    flex: 1,
  },
  textName: {
    color: "#00BF9D",
    fontWeight: "bold",
  },
  containerColorNSize: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 8,
  },
  textColorNSize: {
    color: "#808080",
  },
  color: {
    width: 15,
    height: 15,
    backgroundColor: "black",
    marginRight: 12,
  },
  containerEdit: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 8,
  },
  dropdown: {
    borderWidth: 0.5,
    borderColor: "#808080",
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
  },
  icon: {
    marginLeft: 16,
    color: "#808080",
  },
  containerPrice: {
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 8,
    paddingRight: 16,
  },
});
