import React, { Component } from "react";
import { View, Text } from "react-native";
import { ListItem } from "native-base";
import styles from "./styles";

class BasketPay extends Component {
  render() {
    return (
      <View>
        <ListItem>
          <View style={styles.container}>
            <Text style={styles.text}>Shipping</Text>
            <Text>$6</Text>
          </View>
        </ListItem>
        <ListItem>
          <View style={styles.container}>
            <Text style={styles.text}>Your total</Text>
            <Text>$100</Text>
          </View>
        </ListItem>
      </View>
    );
  }
}

export default BasketPay;
