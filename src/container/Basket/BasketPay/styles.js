import { StyleSheet, Dimensions, Platform } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    padding: 8,
  },
  text: {
    flex: 1,
  },
});
