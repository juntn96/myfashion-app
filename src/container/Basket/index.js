import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Container, Content, List, ListItem, Thumbnail } from "native-base";
import Icon from "../../elements/Icon";
import BasketItem from "./BasketItem";
import BasketPay from "./BasketPay";

import styles from "./styles";

const arr = [{}, {}, {}];


import { connect } from "react-redux";
import * as authActions from "../../store/actions/auth";
import * as commonActions from "../../store/actions/common";

@connect(
  state => ({
    // router: getRouter(state).current,
  }),
  { ...authActions, ...commonActions }
)
class Basket extends Component {

  _onPress = () => {
    const { forwardTo } = this.props
    if (forwardTo) {
      forwardTo('checkout')
    }
  }

  _renderRow = (item) => {
    return (
      <BasketItem />
    )
  }

  render() {
    return (
      <Container>
        <Content style={styles.content}>
          <List
            style={styles.list}
            dataArray={arr}
            renderRow={item => this._renderRow(item)}
          />
          <BasketPay />
          <TouchableOpacity onPress={this._onPress} activeOpacity={0.8} style={styles.button}>
            <Icon name="md-basket" style={styles.buttonIcon} />
            <Text style={styles.buttonText}>Place your order</Text>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}

export default Basket;
