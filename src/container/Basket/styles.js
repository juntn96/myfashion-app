import { StyleSheet, Dimensions, Platform } from "react-native";

export default StyleSheet.create({
  content: {
    backgroundColor: "#f0f0f0",
  },
  list: {
    backgroundColor: "#FFF",
  },
  button: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 48,
    borderRadius: 5,
    marginLeft: 16,
    marginRight: 16,
    marginTop: 8,
    backgroundColor: "#FF894B",
    marginBottom: 24,
  },
  buttonIcon: {
    color: "#FFF",
  },
  buttonText: {
    marginLeft: 12,
    color: "#FFF",
    fontSize: 16,
    fontWeight: "500",
  },
});
