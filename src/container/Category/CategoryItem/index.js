import React, { Component } from "react";
import { View, Text, Image, Dimensions, TouchableOpacity } from "react-native";
import styles from "./styles";

class CategoryItem extends Component {
  _onPress = () => {
    const { onPress, item } = this.props;
    if (onPress) {
      onPress(item);
    }
  };

  render() {
    const { item } = this.props;

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={this._onPress} style={styles.container}>
        <Image style={styles.image} source={item.image} />
        <Text style={styles.text}>{item.name}</Text>
      </TouchableOpacity>
    );
  }
}

export default CategoryItem;
