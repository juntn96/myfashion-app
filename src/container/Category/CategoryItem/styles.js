import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    height: 200,
    backgroundColor: "gray",
    marginBottom: 4,
    marginTop: 4,
    marginLeft: 8,
    marginRight: 8,
    borderRadius: 5,
    justifyContent: "center",
    overflow: "hidden",
    flexWrap: "wrap",
  },
  image: {
    position: "absolute",
    height: 200,
    width: Dimensions.get("screen").width - 16,
    resizeMode: "cover",
  },
  text: {
    fontSize: 22,
    fontWeight: "500",
    marginLeft: 16,
  },
});
