import React, { Component } from "react";
import { View, Image, Dimensions } from "react-native";
import { Content, Container, Text, List } from "native-base";
import items from "./items";
import CategoryItem from "./CategoryItem";
class Category extends Component {
  _onItemPress = item => {
    console.log(item);
  };

  _renderRow = item => {
    return <CategoryItem item={item} onPress={this._onItemPress} />;
  };

  render() {
    return (
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          dataArray={items}
          renderRow={item => this._renderRow(item)}
        />
      </Container>
    );
  }
}

export default Category;
