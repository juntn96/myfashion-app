export default [
  {
    name: "Hat",
    image: require("./images/hat.png"),
  },
  {
    name: "Shirt",
    image: require("./images/shirt.png"),
  },
  {
    name: "Pants",
    image: require('./images/pants.png')
  },
  {
    name: "Dresses",
    image: require('./images/dresses.png')
  },
  {
    name: "Shoes",
    image: require('./images/shoes.png')
  },
];
