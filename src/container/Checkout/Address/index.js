import React, { Component } from "react";
import { View, Text, TextInput, TouchableOpacity } from "react-native";

import styles from "./styles";

import { Container, Content } from "native-base";

class Address extends Component {
  _onNext = () => {
    const { onNext } = this.props;
    if (onNext) {
      onNext(2);
    }
  };

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            <Text style={styles.label}>Your name</Text>
            <View style={styles.gridContainer}>
              <TextInput
                underlineColorAndroid={"transparent"}
                style={[styles.textInput, styles.spaceRight]}
              />
              <TextInput
                underlineColorAndroid={"transparent"}
                style={[styles.textInput, styles.spaceLeft]}
              />
            </View>
            <Text style={styles.label}>Address line</Text>
            <TextInput
              underlineColorAndroid={"transparent"}
              style={styles.textInput}
            />
            <Text style={styles.label}>Address line 2</Text>
            <TextInput
              underlineColorAndroid={"transparent"}
              style={styles.textInput}
            />
            <View style={styles.gridContainer}>
              <View style={styles.contentColumn}>
                <Text style={styles.label}>City</Text>
                <TextInput
                  underlineColorAndroid={"transparent"}
                  style={[styles.textInput, styles.spaceRight]}
                />
              </View>
              <View style={styles.contentColumn}>
                <Text style={[styles.label, styles.spaceLeft]}>Zip code</Text>
                <TextInput
                  underlineColorAndroid={"transparent"}
                  style={[styles.textInput, styles.spaceLeft]}
                />
              </View>
            </View>
            <View style={styles.gridContainer}>
              <View style={styles.contentColumn}>
                <Text style={styles.label}>State</Text>
                <TextInput
                  underlineColorAndroid={"transparent"}
                  style={[styles.spaceRight, styles.textInput]}
                />
              </View>
              <View style={styles.contentColumn}>
                <Text style={[styles.label, styles.spaceLeft]}>Country</Text>
                <TextInput
                  underlineColorAndroid={"transparent"}
                  style={[styles.spaceLeft, styles.textInput]}
                />
              </View>
            </View>

            <Text style={styles.label}>Shipping options</Text>
            <TouchableOpacity activeOpacity={0.8}>
              <Text style={styles.buttonText}>
                Please ship to another address
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this._onNext}
              activeOpacity={0.8}
              style={styles.buttonNext}
            >
              <Text style={styles.btnNextText}>Next Step</Text>
            </TouchableOpacity>
            {/* <View style={styles.footer} /> */}
          </View>
        </Content>
      </Container>
    );
  }
}

export default Address;
