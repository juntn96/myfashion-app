import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    padding: 12,
    flex: 1,
  },
  label: {
    fontSize: 14,
    color: "#808080",
    marginBottom: 8,
    marginTop: 12,
    fontWeight: "600",
  },
  gridContainer: {
    flexDirection: "row",
  },
  textInput: {
    flex: 1,
    borderWidth: 1,
    borderColor: "#CECECE",
    height: 40,
    borderRadius: 5,
    paddingLeft: 8,
    paddingRight: 8,
  },
  spaceLeft: {
    marginLeft: 6,
  },
  spaceRight: {
    marginRight: 6,
  },
  contentColumn: {
    flex: 1,
  },
  buttonText: {
    color: "#FF894D",
  },
  footer: {
    marginBottom: 24,
  },
  buttonNext: {
    height: 46,
    borderRadius: 5,
    backgroundColor: "#FF894D",
    marginTop: 18,
    justifyContent: "center",
    alignItems: "center",
  },
  btnNextText: {
    color: "#FFF",
    fontSize: 17,
    fontWeight: "600",
  },
});
