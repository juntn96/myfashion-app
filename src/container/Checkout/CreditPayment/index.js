import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput, Image } from "react-native";
import { Container, Content } from "native-base";

import Icon from "../../../elements/Icon";

import TermModal from '../TermModal'

import styles from "./styles";

class CreditPayment extends Component {

  constructor(props) {
    super(props)

    this.state = {
      openModal: false
    }
  }

  _openModal = () => {
    this.setState({openModal: true})
  }

  _closeModal = () => {
    this.setState({openModal: false})
  }

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            <View
              style={{
                flex: 1,
              }}
            >
              <TermModal
                closeModal={this._closeModal}
                visible={this.state.openModal}
              />
              <Text style={styles.label}>Scan your card</Text>
              <View
                style={{
                  backgroundColor: "#CECECE",
                  padding: 8,
                  flexDirection: "row",
                  alignItems: "center",
                  borderRadius: 5,
                  marginTop: 8,
                }}
              >
                <Icon name="ios-camera-outline" />
                <Text
                  style={{
                    marginLeft: 16,
                    fontWeight: "500",
                  }}
                >
                  Save time and scan your credit card
                </Text>
              </View>
              <Text style={[styles.label, styles.labelSpace]}>
                Credit card #No
              </Text>
              <View style={styles.gridContainer}>
                <TextInput
                  underlineColorAndroid={"transparent"}
                  style={styles.textInput}
                />
                <Image
                  source={require("../images/vs.png")}
                  style={{
                    marginLeft: 8,
                    marginRight: 8,
                  }}
                />
              </View>

              <View style={styles.gridContainer}>
                <View style={styles.contentColumn}>
                  <Text style={[styles.label, styles.labelSpace]}>Expires</Text>
                  <TextInput
                    underlineColorAndroid={"transparent"}
                    style={[styles.textInput, styles.spaceRight]}
                  />
                </View>
                <View style={styles.contentColumn}>
                  <Text
                    style={[styles.label, styles.spaceLeft, styles.labelSpace]}
                  >
                    CVV-Code
                  </Text>
                  <TextInput
                    underlineColorAndroid={"transparent"}
                    style={[styles.textInput, styles.spaceLeft]}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  marginTop: 12,
                }}
              >
                <View
                  style={{
                    width: 20,
                    height: 20,
                    borderWidth: 0.5,
                    borderColor: "#808080",
                    borderRadius: 5,
                    marginRight: 12,
                  }}
                />
                <View>
                  <Text style={{ fontWeight: "bold" }}>
                    Agree to our terms & conditions
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 8,
                      flexWrap: "wrap",
                    }}
                  >
                    <Text style={styles.label}>
                      I agree that i have read and accepted our{" "}
                    </Text>
                    <TouchableOpacity activeOpacity={0.8} onPress={this._openModal} >
                      <Text
                        style={{
                          color: "#FF894D",
                          fontWeight: "600",
                        }}
                      >
                        terms and conditions
                      </Text>
                    </TouchableOpacity>
                    <Text style={styles.label}> for your purchase</Text>
                  </View>
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={this._onNext}
              activeOpacity={0.8}
              style={styles.buttonNext}
            >
              <Text style={styles.btnNextText}>Finish your order</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

export default CreditPayment;
