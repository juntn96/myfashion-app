import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    padding: 12,
    marginTop: 16,
    flex: 1,
  },
  buttonNext: {
    height: 46,
    borderRadius: 5,
    backgroundColor: "#FF894D",
    marginTop: 18,
    justifyContent: "center",
    alignItems: "center",
  },
  btnNextText: {
    color: "#FFF",
    fontSize: 17,
    fontWeight: "600",
  },
  label: {
    color: "#808080",
    fontWeight: "600",
  },
  gridContainer: {
    flexDirection: "row",
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    borderWidth: 1,
    borderColor: "#CECECE",
    height: 40,
    borderRadius: 5,
    paddingLeft: 8,
    paddingRight: 8,
  },
  labelSpace: {
    marginTop: 16,
    marginBottom: 8,
  },
  spaceLeft: {
    marginLeft: 6,
  },
  spaceRight: {
    marginRight: 6,
  },
  contentColumn: {
    flex: 1,
  },
});
