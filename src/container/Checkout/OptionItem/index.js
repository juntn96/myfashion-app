import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import styles from "./styles";

class OptionItem extends Component {

  _onPress = () => {
    const { onPress, item } = this.props
    if (onPress) {
      onPress(item)
    }
  }

  render() {
    const { item } = this.props;

    const pickColor = item.isPick ? '#66FF33' : '#FFF'

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={this._onPress} style={styles.container}>
        <View style={[styles.square, {backgroundColor: pickColor}]} />
        <View style={styles.content}>
          <Image source={item.logo} />
        </View>
        <View>
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.extend}>{item.extend}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default OptionItem;
