import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 16,
    paddingBottom: 16,
  },
  square: {
    height: 20,
    width: 20,
    borderWidth: 1,
    borderColor: "#CECECE",
    borderRadius: 5,
    marginRight: 16,
  },
  content: {
    flex: 1,
  },
  name: {
    fontSize: 17,
    fontWeight: "500",
    textAlign: "right",
  },
  extend: {
    fontSize: 12,
    color: "#808080",
    textAlign: "right",
    marginTop: 4,
  },
});
