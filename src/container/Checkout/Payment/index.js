import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";

import { List } from "native-base";

import OptionItem from "../OptionItem";

import styles from "./styles";
import items from "./items";

class Payment extends Component {

  _onItemPress = item => {
    console.log(item);
  };

  _onNext = () => {
    const { onNext } = this.props;
    if (onNext) {
      onNext(4)
    }
  }

  _renderRow = item => {
    return <OptionItem item={item} onPress={this._onItemPress} />;
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Which shipping partner do you like</Text>
        <List style={{flex: 1}} dataArray={items} renderRow={item => this._renderRow(item)} />
        <TouchableOpacity onPress={this._onNext} activeOpacity={0.8} style={styles.buttonNext}>
          <Text style={styles.btnNextText}>Next Step</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Payment;
