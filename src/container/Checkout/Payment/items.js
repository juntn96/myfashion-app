export default [
  {
    name: 'Credit Card',
    logo: require('../images/visalogo.png'),
    extend: 'Visa, MasterCard, JCB, Amex',
    isPick: true
  },
  {
    name: 'Paypal',
    logo: require('../images/paypal.png'),
    extend: 'paypalaccount@gmail.com',
    isPick: false
  },
  {
    name: 'ApplePay',
    logo: require('../images/applepay.png'),
    extend: 'applepay@gmail.com',
    isPick: false
  }
]