import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";

import { List } from "native-base";

import OptionItem from "../OptionItem";

import styles from "./styles";
import items from "./items";

class Shipping extends Component {

  _onItemPress = item => {
    console.log(item);
  };

  _onNext = () => {
    const { onNext } = this.props
    if (onNext) {
      onNext(3)
    }
  }

  _renderRow = item => {
    return <OptionItem item={item} onPress={this._onItemPress} />;
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Which shipping partner do you like</Text>
        <View style={styles.spaceTop}>
          <List dataArray={items} renderRow={item => this._renderRow(item)} />
        </View>
        <View style={styles.content}>
          <View style={styles.line} />
          <View style={styles.contentLabel}>
            <Text style={styles.label}>Shipping Address</Text>
          </View>
          <Text>Lam Ngoc Khanh</Text>
          <Text>Ngo 1194 Lang</Text>
          <Text>Apartment 1194</Text>
          <Text>100000 AgileTech</Text>
          <Text>Ha Noi, Viet Nam</Text>
        </View>
        <TouchableOpacity onPress={this._onNext} activeOpacity={0.8} style={styles.buttonNext}>
          <Text style={styles.btnNextText}>Next Step</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Shipping;
