export default [
  {
    name: 'DHL',
    extend: 'No additional Costs',
    logo: require('../images/dhl.png'),
    isPick: false
  },
  {
    name: 'UPS',
    extend: 'No additional Costs',
    logo: require('../images/upslg.png'),
    isPick: false
  },
  {
    name: 'FEDEX EXPRESS',
    extend: 'Additional 12.99$',
    logo: require('../images/fedex.png'),
    isPick: true
  }
]