import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    padding: 12,
    flex: 1,
    marginTop: 16,
  },
  buttonNext: {
    height: 46,
    borderRadius: 5,
    backgroundColor: "#FF894D",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 32,
  },
  btnNextText: {
    color: "#FFF",
    fontSize: 17,
    fontWeight: "600",
  },
  label: {
    color: "#808080",
    fontWeight: "600",
  },
  spaceTop: {
    marginTop: 12,
  },
  content: {
    flex: 1,
  },
  line: {
    height: 1,
    backgroundColor: "#CECECE",
  },
  contentLabel: {
    flexDirection: "row",
    marginTop: 16,
    marginBottom: 18,
  },
});
