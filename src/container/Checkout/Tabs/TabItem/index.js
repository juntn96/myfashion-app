import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";

import styles from "./styles";

class TabItem extends Component {
  _onPress = () => {
    const { onNext, scene } = this.props;
    if (onNext) {
      onNext(scene);
    }
  };

  render() {
    const { title, isActive } = this.props;
    const dotColor = isActive ? "#FF894D" : "transparent";
    const textColor = isActive ? "#FF894D" : "#FFF";

    return (
      <TouchableOpacity
        onPress={this._onPress}
        activeOpacity={0.8}
        style={styles.container}
      >
        <View style={styles.headerContainer}>
          <Text style={[styles.text, { color: textColor }]}>{title}</Text>
        </View>
        <View style={[styles.dot, { backgroundColor: dotColor }]} />
      </TouchableOpacity>
    );
  }
}

export default TabItem;
