import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "transparent",
  },
  headerContainer: {
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#00CDA8",
    width: Dimensions.get("screen").width / 3,
  },
  text: {
    fontSize: 15,
    fontWeight: "bold",
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 10,
    position: "absolute",
    alignSelf: "center",
    bottom: 1,
  },
});
