import React, { Component } from "react";
import { View, Text } from "react-native";

import TabItem from "./TabItem";

import styles from "./styles";

class Tabs extends Component {
  render() {
    const { currentTab, onNext } = this.props;

    return (
      <View style={styles.container}>
        <TabItem
          onNext={onNext}
          scene={1}
          title="Address"
          isActive={currentTab === 1}
        />
        <TabItem
          onNext={onNext}
          scene={2}
          title="Shipping"
          isActive={currentTab === 2}
        />
        <TabItem
          onNext={onNext}
          scene={3}
          title="Payment"
          isActive={currentTab >= 3}
        />
      </View>
    );
  }
}

export default Tabs;
