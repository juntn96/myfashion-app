import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    height: 62,
    backgroundColor: "transparent",
    flexDirection: "row"
  },
});
