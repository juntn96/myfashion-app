import React, { Component } from "react";
import { View, Text, Modal, TouchableOpacity, Platform } from "react-native";
import Icon from "../../../elements/Icon";
import styles from "./styles";

class TermModal extends Component {
  _closeModal = () => {
    const { closeModal } = this.props;

    if (closeModal) {
      closeModal();
    }
  };

  render() {
    const { visible } = this.props;

    return (
      <Modal animationType={"slide"} visible={visible} transparent={true}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.title}>Terms and Conditions</Text>
            <TouchableOpacity
              onPress={this._closeModal}
              activeOpacity={0.8}
              style={styles.closeBtn}
            >
              <Icon name="close" style={styles.icon} />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

export default TermModal;
