import { StyleSheet, Dimensions, Platform } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
    marginTop: Platform.OS === "ios" ? 20 : 0,
  },
  header: {
    height: 56,
    flexDirection: "row",
    backgroundColor: "#00BF9D",
    paddingLeft: 16,
    paddingRight: 16,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    color: "#FFF",
    fontWeight: "600",
    flex: 1,
    textAlign: "center",
  },
  closeBtn: {
    paddingLeft: 8,
  },
  icon: {
    color: "#FFF",
  },
});
