import React, { Component } from "react";
import { View, Text, LayoutAnimation, Dimensions } from "react-native";

import { Container, Content, Tab } from "native-base";

import Address from "./Address";
import Shipping from "./Shipping";
import Payment from "./Payment";
import CreditPayment from './CreditPayment'

import Tabs from "./Tabs";

import styles from "./styles";

class Checkout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scene: 1,
    };
  }

  _onNext = scene => {
    this.setState({ scene: scene });
  };

  _renderScene = () => {
    switch (this.state.scene) {
      case 1: {
        return <Address onNext={this._onNext} />;
      }
      case 2: {
        return <Shipping onNext={this._onNext} />;
      }
      case 3: {
        return <Payment onNext={this._onNext} />;
      }
      case 4: {
        return <CreditPayment onNext={this._onNext} />
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Tabs currentTab={this.state.scene} onNext={this._onNext} />
        {this._renderScene()}
      </View>
    );
  }
}

export default Checkout;
