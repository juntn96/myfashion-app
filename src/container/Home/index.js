import React, { Component } from "react";
import { View, Text, Image, Dimensions, FlatList } from "react-native";
import Swiper from "react-native-swiper";
import { Container, Content, List, ListItem, Thumbnail } from "native-base";
import items from "./items";

import FashionItem from "../../components/FashionItem";

import materials from "../../theme/variables/material";

import styles from "./styles";

import { connect } from "react-redux";
import * as authActions from "../../store/actions/auth";
import * as commonActions from "../../store/actions/common";

@connect(
  state => ({
    // router: getRouter(state).current,
    user: state
  }),
  { ...authActions, ...commonActions }
)
class Home extends Component {

  componentDidMount() {
    console.log('user: ', this.props.user)
  }

  _onItemPress = item => {
    const { forwardTo } = this.props;
    forwardTo("product");
  };

  _renderItem = ({ item }) => {
    return <FashionItem onPress={this._onItemPress} item={item} />;
  };

  render() {
    return (
      <Container>
        <Content>
          <View>
            <View style={styles.banner}>
              <Swiper loop autoplay>
                <View>
                  <Image source={require("../../assets/images/banner1.png")} />
                </View>

                <View>
                  <Image source={require("../../assets/images/banner2.png")} />
                </View>

                <View>
                  <Image source={require("../../assets/images/banner3.png")} />
                </View>
              </Swiper>
            </View>
            <Text style={styles.text}>Discover our topsellers</Text>

            <FlatList
              data={items}
              numColumns={2}
              renderItem={this._renderItem}
              contentContainerStyle={styles.list}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

export default Home;
