import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  banner: {
    height: 300,
  },
  text: {
    fontSize: 17,
    fontWeight: "500",
    textAlign: "center",
    marginTop: 8,
    marginBottom: 8,
  },
  list: {
    justifyContent: "space-between",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
  },
});
