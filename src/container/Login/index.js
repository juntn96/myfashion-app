import React, { Component } from "react";
import { View, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { reduxForm, Field } from "redux-form";
import { Container, Content } from "native-base";
import TextInput from "./TextInput";
import styles from "./styles";

import { connect } from "react-redux";
import * as authActions from "../../store/actions/auth";
import * as commonActions from "../../store/actions/common";

import { validateLogin } from "../../utils/validate";
@connect(
  state => ({
    // router: getRouter(state).current,
    auth: state.auth,
  }),
  { ...authActions, ...commonActions }
)
class Login extends Component {
  _onSubmitPress = values => {
    validateLogin(values);
    const { login, goBack, setToast, auth } = this.props;
    login(values.email, values.password);
    console.log(auth);
    if (auth.error) {
      setToast("login failed");
    } else {
      if (!auth.loading) {
        goBack();
      }
    }
  };

  render() {
    const { handleSubmit, auth } = this.props;

    return (
      <Container>
        <Content contentContainerStyle={{ flex: 1 }}>
          <View style={styles.container}>
            <Text style={styles.title}>SignIn.</Text>
            <View style={styles.content}>
              <Text style={styles.label}>Email</Text>
              <Field
                name="email"
                keyboardType="default"
                placeholder="Email Address"
                secureTextEntry={false}
                component={TextInput}
              />
              <Text style={styles.label}>Password</Text>
              <Field
                name="password"
                keyboardType="default"
                placeholder="Password"
                secureTextEntry={true}
                component={TextInput}
              />
            </View>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={handleSubmit(values => {
                this._onSubmitPress(values);
              })}
              style={styles.buttonNext}
            >
              {auth.loading ? (
                <ActivityIndicator animating color="#FFF" />
              ) : (
                <Text style={styles.btnNextText}>Sign In</Text>
              )}
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

export default reduxForm({
  form: "login",
})(Login);
