import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ECECEC",
    padding: 12,
    paddingBottom: 24,
  },
  buttonNext: {
    height: 46,
    borderRadius: 5,
    backgroundColor: "#FF894D",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 12,
  },
  btnNextText: {
    color: "#FFF",
    fontSize: 17,
    fontWeight: "600",
  },
  content: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    fontSize: 52,
    fontWeight: "600",
  },
  form: {
    marginTop: 24,
  },
  label: {
    fontSize: 12,
    color: "#808080",
    marginTop: 12,
    marginBottom: 8,
  },
  input: {
    padding: 12,
    borderRadius: 5,
    borderWidth: 1,
  },
  extend: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 16,
  },
  easy: {
    fontWeight: "600",
    color: "#808080",
  },
  fb: {
    color: "#FF894D",
    fontWeight: "600",
  },
});
