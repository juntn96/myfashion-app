import React, { Component } from "react";
import { View, Text, Dimensions, TouchableOpacity } from "react-native";

import styles from "./styles";

class OptionButton extends Component {
  _onPress = () => {
    const { option, onPress } = this.props;
    if (onPress) {
      onPress(option);
    }
  };

  render() {
    const { option, isFocus } = this.props;
    const borderColor = isFocus ? "black" : "#CECECE";

    return (
      <TouchableOpacity
        onPress={this._onPress}
        activeOpacity={0.8}
        style={[styles.container, { borderColor: borderColor }]}
      >
        {option.type === "color" ? <View style={styles.color} /> : null}
        <Text style={styles.text}>{option.name}</Text>
      </TouchableOpacity>
    );
  }
}

export default OptionButton;
