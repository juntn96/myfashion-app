import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    borderWidth: 1,
    height: 46,
    width: Dimensions.get("screen").width / 2 - 16,
    borderRadius: 5,
    alignItems: "center",
    flexDirection: "row",
  },
  color: {
    backgroundColor: "black",
    height: 20,
    width: 20,
    marginLeft: 12,
  },
  text: {
    marginLeft: 16,
  },
});
