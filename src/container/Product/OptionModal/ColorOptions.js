export default [
  {
    name: "Black",
    color: "#000",
  },
  {
    name: "Blue",
    color: "#0066ff",
  },
  {
    name: "Yellow",
    color: "#ffff00",
  },
  {
    name: "Green",
    color: "#66ff33",
  },
];
