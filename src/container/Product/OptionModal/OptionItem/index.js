import React, { Component } from "react";
import { View, Text } from "react-native";

import styles from "./styles";

class OptionItem extends Component {
  render() {
    const { item } = this.props;

    return (
      <View style={styles.container}>
        <View
          style={[
            styles.checkbox,
            { borderColor: item.color ? item.color : "#000" },
          ]}
        />
        <Text style={styles.text}>{item.name}</Text>
      </View>
    );
  }
}

export default OptionItem;
