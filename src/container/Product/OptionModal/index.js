import React, { Component } from "react";
import { View, Text, Modal, Dimensions, TouchableOpacity } from "react-native";
import { List } from "native-base";

import OptionItem from "./OptionItem";

import ColorOptions from "./ColorOptions";
import SizeOptions from "./SizeOptions";

import styles from "./styles";

class OptionModal extends Component {
  _renderRow = item => {
    return <OptionItem item={item} />;
  };

  _closeModal = () => {
    const { closeModal } = this.props;
    if (closeModal) {
      closeModal();
    }
  };

  render() {
    const { modalType } = this.props;

    if (modalType) {
      return (
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.props.visible}
        >
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={this._closeModal}
            style={styles.container}
          >
            <View style={styles.popup}>
              <Text style={styles.title}>
                {modalType === "color" ? "Choose a color" : "Choose a size"}
              </Text>
              <List
                dataArray={modalType === "color" ? ColorOptions : SizeOptions}
                renderRow={item => this._renderRow(item)}
              />
            </View>
          </TouchableOpacity>
        </Modal>
      );
    } else {
      return null;
    }
  }
}

export default OptionModal;
