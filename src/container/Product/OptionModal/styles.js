import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  popup: {
    backgroundColor: "#FFFFFF",
    height: Dimensions.get("screen").height / 2.5,
  },
  title: {
    fontSize: 20,
    fontWeight: "500",
    color: "#00BF9D",
    marginTop: 16,
    marginLeft: 8,
    marginBottom: 16,
  },
});
