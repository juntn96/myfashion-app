import React, { Component } from "react";
import { View, Text, Dimensions } from "react-native";
import styles from "./styles";

class ProductDetail extends Component {
  render() {
    return (
      <View>
        <Text style={[styles.title, styles.space]}>Description</Text>
        <Text style={styles.spaceDescription}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged
        </Text>
        <View style={styles.container}>
          <View style={styles.childContainer}>
            <Text style={styles.title}>Available Colors</Text>
            <View style={styles.listColors}>
              <View style={[styles.color, { backgroundColor: "red" }]} />
              <View style={[styles.color, { backgroundColor: "blue" }]} />
              <View style={[styles.color, { backgroundColor: "green" }]} />
            </View>
          </View>
          <View style={styles.childContainer}>
            <Text style={styles.title}>Available Sizes</Text>
            <Text>S, M, L, XXL</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default ProductDetail;
