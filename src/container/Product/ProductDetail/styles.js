import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  title: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#00BF9D",
    marginBottom: 4,
  },
  space: {
    marginLeft: 8,
    marginTop: 12,
    marginRight: 8,
  },
  spaceDescription: {
    marginLeft: 8,
    marginRight: 8,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 16,
  },
  childContainer: {
    width: Dimensions.get("screen").width / 2 - 16,
    height: 48,
  },
  listColors: {
    flexDirection: "row",
  },
  color: {
    width: 20,
    height: 20,
    marginRight: 8,
  },
});
