import React, { Component } from "react";
import { View, Text } from "react-native";
import styles from "./styles";

class ProductReview extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.avatarContainer} />
        <View style={styles.reviewContainer}>
          <View style={styles.header}>
            <Text>Lam Ngoc Khanh</Text>
          </View>
          <Text style={styles.textTime}>2 hours ago</Text>
          <Text>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged
          </Text>
        </View>
      </View>
    );
  }
}

export default ProductReview;
