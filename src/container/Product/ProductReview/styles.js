import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    marginTop: 12,
    marginRight: 8,
  },
  avatarContainer: {
    width: 50,
    height: 50,
    backgroundColor: "gray",
    marginLeft: 8,
    marginRight: 12,
    borderRadius: 50,
  },
  reviewContainer: {
    flex: 1,
  },
  header: {
    flexDirection: "row",
  },
  textTime: {
    color: "#808080",
    fontSize: 12,
    marginTop: 4,
    marginBottom: 4,
  },
});
