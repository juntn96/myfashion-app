import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styles from "./styles";

import { connect } from "react-redux";
import * as authActions from "../../../store/actions/auth";
import * as commonActions from "../../../store/actions/common";

@connect(
  state => ({
    // router: getRouter(state).current,
    user: state.auth.user,
  }),
  { ...authActions, ...commonActions }
)
class PurchaseButton extends Component {
  _onPress = () => {
    const { onPress, item, forwardTo, user } = this.props;
    if (!user) {
      forwardTo("welcome");
    } else {
      // TODO: purchase
    }
  };

  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={this._onPress}
        style={styles.container}
      >
        <Text style={styles.text}>Purchase</Text>
      </TouchableOpacity>
    );
  }
}

export default PurchaseButton;
