import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    width: Dimensions.get("screen").width / 2 - 16,
    backgroundColor: "#FF894B",
    height: 48,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: "#FFF",
    fontSize: 17,
    fontWeight: "500",
  },
});
