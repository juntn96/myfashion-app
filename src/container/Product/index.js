import React, { Component } from "react";
import { View, Text, Dimensions, TouchableOpacity } from "react-native";
import Swiper from "react-native-swiper";
import { Container, Content, List } from "native-base";

import OptionButton from "./OptionButton";
import OptionModal from "./OptionModal";
import ProductReview from "./ProductReview";
import ProductDetail from "./ProductDetail";
import PurchaseButton from "./PurchaseButton";

import styles from "./styles";

import { connect } from "react-redux";
import * as authActions from "../../store/actions/auth";
import * as commonActions from "../../store/actions/common";

@connect(
  state => ({
    // router: getRouter(state).current,
  }),
  { ...authActions, ...commonActions }
)
class Product extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFocusColor: false,
      isFocusSize: false,
      modalVisible: false,
      colorOption: {},
      sizeOption: {},
    };
  }

  _onOptionPress = option => {
    console.log(option);
    if (option.type === "color") {
      this.setState({ isFocusColor: true, isFocusSize: false });
      this._openModal("color");
    } else {
      this.setState({ isFocusColor: false, isFocusSize: true });
      this._openModal("size");
    }
  };

  _openModal = modalType => {
    this.setState({ modalType: modalType, modalVisible: true });
  };

  _closeModal = () => {
    this.setState({
      modalVisible: false,
      isFocusColor: false,
      isFocusSize: false,
    });
  };

  render() {
    return (
      <Container>
        <Content>
          <OptionModal
            modalType={this.state.modalType}
            visible={this.state.modalVisible}
            closeModal={this._closeModal}
          />
          <View style={styles.swiper}>
            <Swiper>
              <View />
              <View />
              <View />
            </Swiper>
          </View>
          <View style={styles.rowGrid}>
            <OptionButton
              onPress={this._onOptionPress}
              isFocus={this.state.isFocusColor}
              option={{
                type: "color",
                name: "Black",
              }}
            />
            <OptionButton
              onPress={this._onOptionPress}
              isFocus={this.state.isFocusSize}
              option={{
                type: "size",
                name: "Small",
              }}
            />
          </View>
          <View style={[styles.rowGrid, styles.gridSpace]}>
            <View style={styles.priceContainer}>
              <Text style={styles.textPrice}>$120</Text>
              <Text style={styles.textSale}>$80</Text>
            </View>
            <PurchaseButton />
          </View>
          <View style={styles.line} />
          <ProductDetail />
          <View style={styles.numberReviewContainer}>
            <Text>33 Reviews</Text>
          </View>
          <ProductReview />
          <View style={styles.footerSpace} />
        </Content>
      </Container>
    );
  }
}

export default Product;
