import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  swiper: {
    height: Dimensions.get("screen").height / 1.5,
    backgroundColor: "black",
    marginBottom: 8,
  },
  rowGrid: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  gridSpace: {
    marginTop: 16,
  },
  priceContainer: {
    flexDirection: "row",
                width: Dimensions.get("screen").width / 2 - 16,
                alignItems: "center",
  },
  textPrice: {
    color: "#000",
                  fontWeight: "400",
                  fontSize: 17,
  },
  textSale: {
    color: "#808080",
                  fontWeight: "400",
                  fontSize: 15,
                  textDecorationLine: "line-through",
                  marginLeft: 12,
  },
  line: {
    backgroundColor: "#CECECE",
              height: 1,
              marginTop: 12,
  },
  numberReviewContainer: {
    backgroundColor: "#F0F0F0",
              paddingLeft: 16,
              paddingBottom: 4,
              paddingTop: 4,
  },
  footerSpace: {
    marginBottom: 24 
  }
});
