import React, { Component } from "react";
import { View, Text, TextInput } from "react-native";
import styles from "./styles";

export default ({
  keyboardType,
  placeholder,
  secureTextEntry,
  meta: { touched, error, warning },
  input: { onChange, ...restInput },
}) => {
  const bdColor = touched && error ? "red" : "#808080";
  return (
    <TextInput
      underlineColorAndroid={"transparent"}
      keyboardType={keyboardType}
      autoCorrect={false}
      placeholder={placeholder}
      secureTextEntry={secureTextEntry}
      onChange={onChange}
      {...restInput}
      style={[styles.input, { borderColor: bdColor }]}
    />
  );
};
