import React, { Component } from "react";
import { View, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { Container, Content } from "native-base";
import TextInput from "./TextInput";

import styles from "./styles";

import { reduxForm, Field } from "redux-form";

import { validateRegister } from "../../utils/validate";

import { connect } from "react-redux";
import * as authActions from "../../store/actions/auth";
import * as commonActions from "../../store/actions/common";

@connect(
  state => ({
    // router: getRouter(state).current,
    auth: state.auth,
  }),
  { ...authActions, ...commonActions }
)
class Register extends Component {
  _onSubmit = values => {
    validateRegister(values);
    const { register, goBack, forwardTo, auth } = this.props;
    register(values.name, values.email, values.password);
    if (!auth.loading) {
      forwardTo('login')
    }
  };

  render() {
    const { handleSubmit, submitting, onSubmit, auth } = this.props;
    console.log(auth.loading);
    return (
      <Container>
        <Content scrollEnabled={false} contentContainerStyle={{ flex: 1 }}>
          <View style={styles.container}>
            <View style={styles.content}>
              <Text style={styles.title}>Signup.</Text>
              <View style={styles.form}>
                <Text style={styles.label}>Your name</Text>
                <Field
                  name="name"
                  keyboardType="default"
                  placeholder="Your full name"
                  secureTextEntry={false}
                  component={TextInput}
                />
                <Text style={styles.label}>Your email address</Text>
                <Field
                  name="email"
                  keyboardType="default"
                  placeholder="Email Address"
                  secureTextEntry={false}
                  component={TextInput}
                />
                <Text style={styles.label}>Your password</Text>
                <Field
                  name="password"
                  keyboardType="default"
                  placeholder="Password"
                  secureTextEntry={true}
                  component={TextInput}
                />
                <View style={styles.extend}>
                  <Text style={styles.easy}>Or easily</Text>
                  <Text style={styles.fb}> connect with facebook</Text>
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={handleSubmit(values => {
                this._onSubmit(values);
              })}
              disabled={auth.loading}
              activeOpacity={0.8}
              style={styles.buttonNext}
            >
              {auth.loading ? (
                <ActivityIndicator animating color="#FFF" />
              ) : (
                <Text style={styles.btnNextText}>Complete Registration</Text>
              )}
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

export default reduxForm({
  form: "register",
})(Register);
