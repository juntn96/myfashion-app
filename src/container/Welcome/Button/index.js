import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styles from "./styles";

import { connect } from "react-redux";
import * as authActions from "../../../store/actions/auth";
import * as commonActions from "../../../store/actions/common";

@connect(
  state => ({
    // router: getRouter(state).current,
  }),
  { ...authActions, ...commonActions }
)
class Button extends Component {

  _onPress = () => {
    const { forwardTo, type } = this.props
    if (forwardTo) {
      forwardTo(type)
    }
  }

  render() {
    const { type } = this.props;

    const btnStyle =
      type === "register"
        ? {
            marginLeft: 16,
            marginRight: 8,
            backgroundColor: "#FF894D",
          }
        : {
            marginLeft: 8,
            marginRight: 16,
            backgroundColor: "#00BF9D",
          };

    const title = type === "register" ? "Register" : "Login";

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={this._onPress} style={[styles.container, btnStyle]}>
        <Text style={styles.text}>{title}</Text>
      </TouchableOpacity>
    );
  }
}

export default Button;
