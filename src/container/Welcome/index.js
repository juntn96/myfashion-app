import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  Platform,
} from "react-native";
import Icon from "../../elements/Icon";
import styles from "./styles";
import Button from './Button'
class Welcome extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require("../../assets/images/bg.png")}
          style={styles.background}
        />
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Fashion</Text>
        </View>
        <View style={styles.footer}>
          <Button type="register" />
          <Button type="login" />
        </View>
      </View>
    );
  }
}

export default Welcome;
