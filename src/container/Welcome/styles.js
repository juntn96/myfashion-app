import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    position: "absolute",
    height: Dimensions.get("screen").height,
    width: Dimensions.get("screen").width,
    resizeMode: "cover",
  },
  titleContainer: {
    flex: 1,
    alignItems: "center",
  },
  title: {
    fontSize: 56,
    fontWeight: "900",
    marginTop: Dimensions.get("screen").height / 4,
  },
  footer: {
    flexDirection: "row",
    marginBottom: 36,
  },
});
