import Login from "./container/Login";
import Register from "./container/Register"
import Welcome from "./container/Welcome"

import Home from "./container/Home";
import Category from "./container/Category";
import Product from "./container/Product";
import Basket from "./container/Basket";
import Checkout from './container/Checkout'

export default {
  login: {
    title: "Login",
    Page: Login,
    headerType: "back",
    footerType: "none",
    cache: true,
  },
  register: {
    title: "Sign Up",
    Page: Register,
    headerType: "back",
    footerType: "none",
    cache: true,
  },
  welcome: {
    title: "Welcome",
    Page: Welcome,
    headerType: "back",
    footerType: "none",
    cache: true,
  },
  home: {
    title: "Home",
    Page: Home,
    headerType: "home",
    footerType: "none",
    cache: true,
  },
  category: {
    title: "Categories",
    Page: Category,
    headerType: "home",
    footerType: "none",
    cache: true,
  },
  product: {
    title: "Product",
    Page: Product,
    headerType: "home",
    footerType: "none",
    cache: true,
  },
  basket: {
    title: "Basket",
    Page: Basket,
    headerType: "home",
    footerType: "none",
    cache: true,
  },
  checkout: {
    title: "Checkout",
    Page: Checkout,
    headerType: "home",
    footerType: "none",
    cache: true,
  }
};
