import {
  APP_LOGIN,
  APP_LOGOUT,
  APP_REGISTER,
  APP_AUTH_REQUESTING,
  APP_LOGIN_SUCCESS,
  APP_LOGOUT_SUCCESS,
  APP_REGISTER_SUCCESS,
  APP_AUTH_REQUEST_FAILED,
} from "../../constants/types";

export const login = (email, password) => ({
  type: APP_LOGIN,
  payload: {
    email,
    password,
  },
});

export const register = (name, email, password) => ({
  type: APP_REGISTER,
  payload: {
    name,
    email,
    password
  }
})

export const logout = () => ({ type: APP_LOGOUT });

export const loginSuccess = user => ({
  type: APP_LOGIN_SUCCESS,
  payload: {
    user,
  },
});

export const logoutSuccess = () => ({ type: APP_LOGOUT_SUCCESS });

export const registerSuccess = () => ({ type: APP_REGISTER_SUCCESS })

export const authRequestFailed = error => ({
  type: APP_AUTH_REQUEST_FAILED,
  payload: {
    error,
  },
});
