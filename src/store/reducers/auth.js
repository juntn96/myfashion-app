import {
  APP_LOGIN,
  APP_LOGOUT,
  APP_REGISTER,
  APP_LOGIN_SUCCESS,
  APP_LOGOUT_SUCCESS,
  APP_REGISTER_SUCCESS,
  APP_AUTH_REQUEST_FAILED,
} from "../../constants/types";

const init = {
  user: null,
  loading: false,
  error: null,
};

export default (state = init, { type, payload }) => {
  switch (type) {
    case APP_LOGIN:
    case APP_LOGOUT:
    case APP_REGISTER: {
      return { ...state, loading: true };
    }
    case APP_LOGIN_SUCCESS: {
      return { ...state, user: payload.user, loading: false, error: null };
    }
    case APP_LOGOUT_SUCCESS: {
      return { ...state, user: null, loading: false, error: null };
    }
    case APP_REGISTER_SUCCESS: {
      return { ...state, loading: false, error: null };
    }
    case APP_AUTH_REQUEST_FAILED: {
      return { ...state, loading: false, error: payload.error };
    }
    default:
      return state;
  }
};
