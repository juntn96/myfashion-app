import { takeLatest, all, put } from "redux-saga/effects";
import {
  APP_LOGIN,
  APP_LOGOUT,
  APP_REGISTER,
  APP_LOGIN_SUCCESS,
  APP_LOGOUT_SUCCESS,
  APP_REGISTER_SUCCESS,
  APP_AUTH_REQUEST_FAILED,
} from "../../constants/types";
import auth from "../api/auth";
import { setToast, resetTo, closeDrawer } from "../actions/common";
import {
  loginSuccess,
  logoutSuccess,
  authRequestFailed,
  registerSuccess,
} from "../actions/auth";
import { createRequestSaga } from "./common";

import firebase from "react-native-firebase";

const requestLogin = function*({ payload }) {
  console.log("request login:, ", payload);
  try {
    const user = yield firebase
      .auth()
      .signInAndRetrieveDataWithEmailAndPassword(
        payload.email,
        payload.password
      );
    if (user) {
      yield put(loginSuccess(user.user));
    } else {
      yield put(authRequestFailed("login failed"));
    }
  } catch (error) {
    yield put(authRequestFailed(error));
  }
};

const requestLogout = function*() {
  console.log("request logout");
  try {
    yield firebase.auth().signOut();
    yield put(logoutSuccess());
  } catch (error) {
    yield put(authRequestFailed(error));
  }
};

const requestRegister = function*({ payload }) {
  console.log("request register: ", payload);
  try {
    const user = yield firebase
      .auth()
      .createUserAndRetrieveDataWithEmailAndPassword(
        payload.email,
        payload.password
      );
    if (user) {
      console.log(user)
      yield user.user.updateProfile({
        displayName: payload.name,
      });
      yield put(registerSuccess());
    } else {
      yield put(authRequestFailed("register failed"));
    }
  } catch (error) {
    yield put(authRequestFailed(error));
  }
};

const fetchWatcher = function*() {
  yield all([
    takeLatest(APP_LOGIN, requestLogin),
    takeLatest(APP_LOGOUT, requestLogout),
    takeLatest(APP_REGISTER, requestRegister),
  ]);
};

// root saga reducer
export default [fetchWatcher];
