import { SubmissionError } from 'redux-form'

export const validateRegister = values => {
  console.log(values)
  if (!values.name) {
    throw new SubmissionError({name: 'length too short', _error: 'register failed'})
  }
  else if (!values.email) {
    throw new SubmissionError({email: 'length too short', _error: 'register failed'})
  }
  else if (!values.password) {
    throw new SubmissionError({password: 'length too short', _error: 'register failed'})
  }
}

export const validateLogin = values => {
  console.log(values)
  if (!values.email) {
    throw new SubmissionError({email: 'length too short', _error: 'register failed'})
  }
  else if (!values.password) {
    throw new SubmissionError({password: 'length too short', _error: 'register failed'})
  }
}